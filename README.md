# Build Image

```bash
docker build -t bemiteu/web-php .
docker image save -o img.tar bemiteu/web-php

# Start Container and open bash
docker run --name webbuild --rm -i -t bemiteu/web-php bash
```